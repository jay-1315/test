const { finaldata} = require('../db/querys/listdata')

const getData = async(ctx) =>{

  const hid = ctx.request.query.hid
  try{    
    const query1 = await finaldata(+hid)    
    ctx.status = 200  
    ctx.body = {...query1[0]}

  }catch(e){
    console.log(e);
  }
}

module.exports = getData;