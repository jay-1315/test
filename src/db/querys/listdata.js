const Myconnection = Database.collection('hospital');

const finaldata = (hid) => Myconnection.aggregate([
  {
      '$match': {
          'hospitalCode': hid
      }
  }, {
      '$unwind': {
          'path': '$totalVacsinStorage'
      }
  }, {
      '$lookup': {
          'from': 'citizen', 
          'let': {
              'vaccinationscode': '$totalVacsinStorage.code'
          }, 
          'pipeline': [
              {
                  '$unwind': '$vaccinations'
              }, {
                  '$match': {
                      '$expr': {
                          '$and': [
                              {
                                  '$eq': [
                                      '$$vaccinationscode', '$vaccinations.code'
                                  ]
                              }, {
                                  '$eq': [
                                      '$vaccinations.isVacsinated', true
                                  ]
                              }
                          ]
                      }
                  }
              }, {
                  '$count': 'people'
              }
          ], 
          'as': 'people'
      }
  }, {
      '$lookup': {
          'from': 'vaccinationsData', 
          'let': {
              'vaccinationscode': '$totalVacsinStorage.code'
          }, 
          'pipeline': [
              {
                  '$match': {
                      '$expr': {
                          '$and': [
                              {
                                  '$eq': [
                                      '$$vaccinationscode', '$code'
                                  ]
                              }
                          ]
                      }
                  }
              }, {
                  '$project': {
                      '_id': 0, 
                      'name': 1, 
                      'code': 1
                  }
              }
          ], 
          'as': 'totalVaccineStorage'
      }
  }, {
      '$addFields': {
          'totalVaccineStorage.people': {
              '$cond': {
                  'if': {
                      '$ne': [
                          '$people', []
                      ]
                  }, 
                  'then': {
                      '$arrayElemAt': [
                          '$people.people', 0
                      ]
                  }, 
                  'else': 0
              }
          }
      }
  }, {
      '$lookup': {
          'from': 'vaccinationsData', 
          'let': {
              'vCode': '$totalVacsinStorage.code'
          }, 
          'pipeline': [
              {
                  '$match': {
                      '$expr': {
                          '$and': [
                              {
                                  '$eq': [
                                      '$$vCode', '$code'
                                  ]
                              }
                          ]
                      }
                  }
              }, {
                  '$project': {
                      '_id': 0, 
                      'name': 1, 
                      'code': 1
                  }
              }
          ], 
          'as': 'remainingVaccine'
      }
  }, {
      '$unwind': {
          'path': '$bookedSlot'
      }
  }, {
      '$addFields': {
          'remainingVaccine.count': {
              '$subtract': [
                  '$totalVacsinStorage.count', {
                      '$cond': {
                          'if': {
                              '$eq': [
                                  '$totalVacsinStorage.code', '$bookedSlot.code'
                              ]
                          }, 
                          'then': '$bookedSlot.count', 
                          'else': 0
                      }
                  }
              ]
          }
      }
  }, {
      '$group': {
          '_id': '$_id', 
          'hospitalName': {
              '$first': '$name'
          }, 
          'address': {
              '$first': {
                  '$concat': [
                      '$address.city', ', ', '$address.country', ', ', {
                          '$toString': '$address.zipcode'
                      }
                  ]
              }
          }, 
          'totalVacsinStorage': {
              '$push': {
                  '$arrayElemAt': [
                      '$totalVaccineStorage', 0
                  ]
              }
          }, 
          'remainingVaccine': {
              '$push': {
                  '$arrayElemAt': [
                      '$remainingVaccine', 0
                  ]
              }
          }
      }
  }, {
      '$project': {
          '_id': 0
      }
  }
]).toArray()

const hostiptalCheck = (hid) => Myconnection.findOne({ hospitalCode : hid }) 

module.exports = { finaldata , hostiptalCheck }