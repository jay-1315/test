let http = require('http')
const koa = require('koa')
const bodyparser = require('koa-bodyparser');

const app =  new koa();
app.use(bodyparser());

const port = process.env.PORT || '3000'
mongoDB = module.exports = require('./src/db/index');
Database = module.exports =  mongoDB.db('drive12')

app.use(require('./src/routers/index').routes());

http.createServer(app.callback()).listen(port, (err) => {
    if (err) {
      console.log('Error occured on starting the server')
      console.log(err)
      return
    }
    console.log(`Server running successfully on port: ${port}`)
  })